﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

/// <summary>
/// UI Audio Settings view.
/// </summary>
public class UIAudioSettings : MonoBehaviour
{
    // Reference to sound volume slider.
    [SerializeField]
    private Slider soundSlider;
    // Action called when sound volume is changed.
    public UnityAction<float> OnSoundVolumeChanged;

    // Reference to music volume slider.
    [SerializeField]
    private Slider musicSlider;
    // Action called when music volume is changed.
    public UnityAction<float> OnMusicVolumeChanged;

    /// <summary>
    /// Unity method called on component enable.
    /// Used to attach listeners to sliders.
    /// </summary>
    private void OnEnable()
    {
        soundSlider.onValueChanged.AddListener(ChangeSoundVolume);
        musicSlider.onValueChanged.AddListener(ChangeMusicVolume);
    }

    /// <summary>
    /// Unity method called on component disable.
    /// Used to attach listeners to sliders.
    /// </summary>
    private void OnDisable()
    {
        soundSlider.onValueChanged.RemoveListener(ChangeSoundVolume);
        musicSlider.onValueChanged.RemoveListener(ChangeMusicVolume);
    }

    /// <summary>
    /// Invokes OnSoundVolumeChanged action with received parameter.
    /// </summary>
    /// <param name="value">Sound Volume Value. (0.0001 - 1)</param>
    public void ChangeSoundVolume(float value)
    {
        OnSoundVolumeChanged?.Invoke(value);
    }

    /// <summary>
    /// Invokes OnMusicVolumeChanged action with received parameter.
    /// </summary>
    /// <param name="value">Music Volume Value. (0.0001 - 1)</param>
    public void ChangeMusicVolume(float value)
    {
        OnMusicVolumeChanged?.Invoke(value);
    }
}
