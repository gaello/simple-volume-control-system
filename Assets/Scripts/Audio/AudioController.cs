﻿using UnityEngine;
using UnityEngine.Audio;

/// <summary>
/// Audio controller.
/// Changes volumes of different audio groups.
/// </summary>
public class AudioController : MonoBehaviour
{
    // Reference to the master audio mixer.
    [SerializeField]
    private AudioMixer masterMixer;

    // Parameters names.
    private const string SOUND_VOLUME_KEY = "Sound Volume";
    private const string MUSIC_VOLUME_KEY = "Music Volume";

    /// <summary>
    /// Changes volume in master mixer.
    /// </summary>
    /// <param name="volume">Sound Volume. Values: 0.0001 - 1</param>
    public void ChangeSoundVolume(float volume)
    {
        // Converting volume from linear to a logarithmic scale.
        volume = Mathf.Log(volume) * 20;
        masterMixer.SetFloat(SOUND_VOLUME_KEY, volume);
    }

    /// <summary>
    /// Changes volume in master mixer.
    /// </summary>
    /// <param name="volume">Music Volume. Values: 0.0001 - 1</param>
    public void ChangeMusicVolume(float volume)
    {
        // Converting volume from linear to a logarithmic scale.
        volume = Mathf.Log(volume) * 20;
        masterMixer.SetFloat(MUSIC_VOLUME_KEY, volume);
    }
}
