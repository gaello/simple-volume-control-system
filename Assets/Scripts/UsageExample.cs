﻿using UnityEngine;

/// <summary>
/// This class connects View and Controller.
/// </summary>
public class UsageExample : MonoBehaviour
{
    // Reference to audio controller.
    [SerializeField]
    private AudioController audioController;

    // Reference to ui audio settings view.
    [SerializeField]
    private UIAudioSettings audioSettings;

    /// <summary>
    /// Unity method called on component enable.
    /// Used here to attach actions.
    /// </summary>
    private void OnEnable()
    {
        audioSettings.OnSoundVolumeChanged += audioController.ChangeSoundVolume;
        audioSettings.OnMusicVolumeChanged += audioController.ChangeMusicVolume;
    }

    /// <summary>
    /// Unity method called on component disable.
    /// Used here to attach actions.
    /// </summary>
    private void OnDisable()
    {
        audioSettings.OnSoundVolumeChanged -= audioController.ChangeSoundVolume;
        audioSettings.OnMusicVolumeChanged -= audioController.ChangeMusicVolume;
    }
}
